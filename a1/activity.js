// Count the total number of fruits on sale.
db.fruits.aggregate([
        {$match: {onSale: true}},
        {$count: "fruitsOnSale"}
    ]);

// Count the total number of fruits with stock more than 20.

db.fruits.aggregate([
        {$match: { stock: { $gte: 20 } } },
        {$count: "enoughStock"}
    ]);

// average operator to get the average price of fruits onSale per supplier.
 db.fruits.aggregate([
        {$match: {onSale: true}},
        { $group: { _id: "$supplier_id", avg_price: {$avg: "$price"} } },
        { $sort: {total: -1} }
    ]);

// max operator to get the highest price of a fruit per supplier.
    db.fruits.aggregate([
        {$match: {onSale: true}},
        { $group: { _id: "$supplier_id", max_price: {$max: "$price"} } },
        { $sort: {total: 1} }
    ]);

//  min operator to get the lowest price of a fruit per supplier.
    db.fruits.aggregate([
        {$match: {onSale: true}},
        { $group: { _id: "$supplier_id", min_price: {$min: "$price"} } },
        { $sort: {total: 1} }
    ]);
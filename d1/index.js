db.fruits.insertMany([
{
name : "Apple",
color : "Red",
stock : 20,
price: 40,
supplier_id : 1,
onSale : true,
origin: [ "Philippines", "US" ]
},
{
name : "Banana",
color : "Yellow",
stock : 15,
price: 20,
supplier_id : 2,
onSale : true,
origin: [ "Philippines", "Ecuador" ]
},
{
name : "Kiwi",
color : "Green",
stock : 25,
price: 50,
supplier_id : 1,
onSale : true,
origin: [ "US", "China" ]
},
{
name : "Mango",
color : "Yellow",
stock : 10,
price: 120,
supplier_id : 2,
onSale : false,
origin: [ "Philippines", "India" ]
}
]);

// AGGREGATION

/*
AGGREGATE METHODS
1.match
2.groups
3.sort
4.project

AGGREGATION PIPELINES

OPERATORS
1.sum
2.max
3.min
4.avg
*/

// MONGODB AGGREGATION
/*
-Used to generate manipulated data and perform operations to create filtered results that help in analyzing data.
-compared to doing CRUD operations on our data from our previous sessions, aggregations gives us access to manipulate, filter and compute for results, providing us with imformation to make necessary development decisions.
*/


// AGGREGATE MEHTHODS
/*
1.MATCH "$match"
- is used to pass the documents that meet the specified condition to the next pipeline..
*/

db.fruits.aggregate([
	{$match: {onSale: true}}
]);

db.fruits.aggregate([{$match: {stock: {$gte: 20}}}
]);


/*
2.GROUP "$group"
- is used to group elements together field-value pairs using data from the grouped element
*/

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}
}
]);


/*
3.SORT "$sort"
- can be used when changing the order of aggregated results
-providing a value of -1 will sort descending order
-providing a value of 1 will sort ascending order
*/

db.fruits.aggregate([
	{$match: { onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort: {total: -1}}
	]);

/*
4.PROJECT: "$project"
-can be used when aggregating data to include or exclude fields from the returned results.
1 -- include
0 -- exclude
*/

db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
{$project: {_id: 0}}
]);


db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
{$project: {_id: 0}},
{$sort: {total: -1}}
]);


/*
AGGREGATION PIPELINES
-the aggregation pipeline i mongoDB is a framework for data aggregation
-each stage transforms the documents as they pass through the deadlines.
-Syntax:
db.collectionName.aggregate([
{stage:A},
{stage:B},
{stage:C},
])
*/


// OPERATORS
// 1. SUM "$sum" - get the total of everything
db.fruits.aggregate([
{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

db.fruits.aggregate([
{$group: {_id: null, total: {$sum: "$stock"}}}
]);

// 2. MAX "$max" - get the highest value out of everything else

db.fruits.aggregate([
{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
]);

// 3. MIN "$min" - gets the lowest value out of everything else

db.fruits.aggregate([
{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
]);

// 4. AVG "$avg" - gets th average value of all the fields

db.fruits.aggregate([
{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}
]);